﻿
namespace PZFR_Tarea_01_Hola_Mundo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.HolaButtom = new System.Windows.Forms.Button();
            this.AdiósButtom = new System.Windows.Forms.Button();
            this.Lblmensaje2 = new System.Windows.Forms.Label();
            this.Lblmensaje = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.HolaButtom, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.AdiósButtom, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Lblmensaje2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Lblmensaje, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(403, 157);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // HolaButtom
            // 
            this.HolaButtom.BackColor = System.Drawing.Color.YellowGreen;
            this.HolaButtom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HolaButtom.Font = new System.Drawing.Font("Montserrat", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HolaButtom.ForeColor = System.Drawing.Color.Black;
            this.HolaButtom.Location = new System.Drawing.Point(3, 3);
            this.HolaButtom.Name = "HolaButtom";
            this.HolaButtom.Size = new System.Drawing.Size(195, 72);
            this.HolaButtom.TabIndex = 0;
            this.HolaButtom.Text = "HOLA";
            this.HolaButtom.UseVisualStyleBackColor = false;
            this.HolaButtom.Click += new System.EventHandler(this.HolaButtom_Click);
            // 
            // AdiósButtom
            // 
            this.AdiósButtom.BackColor = System.Drawing.Color.YellowGreen;
            this.AdiósButtom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AdiósButtom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdiósButtom.Font = new System.Drawing.Font("Montserrat", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdiósButtom.ForeColor = System.Drawing.Color.Black;
            this.AdiósButtom.Location = new System.Drawing.Point(204, 81);
            this.AdiósButtom.Name = "AdiósButtom";
            this.AdiósButtom.Size = new System.Drawing.Size(196, 73);
            this.AdiósButtom.TabIndex = 1;
            this.AdiósButtom.Text = "ADIÓS";
            this.AdiósButtom.UseVisualStyleBackColor = false;
            this.AdiósButtom.Click += new System.EventHandler(this.AdiósButtom_Click);
            // 
            // Lblmensaje2
            // 
            this.Lblmensaje2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Lblmensaje2.Font = new System.Drawing.Font("Yu Gothic UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lblmensaje2.Location = new System.Drawing.Point(3, 78);
            this.Lblmensaje2.Name = "Lblmensaje2";
            this.Lblmensaje2.Size = new System.Drawing.Size(195, 79);
            this.Lblmensaje2.TabIndex = 2;
            this.Lblmensaje2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lblmensaje
            // 
            this.Lblmensaje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Lblmensaje.Font = new System.Drawing.Font("Yu Gothic UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lblmensaje.ForeColor = System.Drawing.Color.Black;
            this.Lblmensaje.Location = new System.Drawing.Point(204, 0);
            this.Lblmensaje.Name = "Lblmensaje";
            this.Lblmensaje.Size = new System.Drawing.Size(196, 78);
            this.Lblmensaje.TabIndex = 3;
            this.Lblmensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__8_;
            this.pictureBox9.Location = new System.Drawing.Point(268, 215);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(54, 43);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 9;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.cactus__1_;
            this.pictureBox8.Location = new System.Drawing.Point(140, 238);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(73, 52);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 8;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.cactus__2_;
            this.pictureBox7.Location = new System.Drawing.Point(149, 12);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(50, 54);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__4_;
            this.pictureBox6.Location = new System.Drawing.Point(338, 226);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(66, 64);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 6;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__7_;
            this.pictureBox5.Location = new System.Drawing.Point(299, -4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(45, 56);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__6_;
            this.pictureBox4.Location = new System.Drawing.Point(104, 135);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(28, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.cactus;
            this.pictureBox3.Location = new System.Drawing.Point(1, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(51, 57);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__2_;
            this.pictureBox2.Location = new System.Drawing.Point(359, 112);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PZFR_Tarea_01_Hola_Mundo.Properties.Resources.planta__3_;
            this.pictureBox1.Location = new System.Drawing.Point(4, 225);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(403, 293);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(419, 332);
            this.MinimumSize = new System.Drawing.Size(419, 332);
            this.Name = "Form1";
            this.Text = "Hola Mundo PCP 1-1";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button HolaButtom;
        private System.Windows.Forms.Button AdiósButtom;
        private System.Windows.Forms.Label Lblmensaje2;
        private System.Windows.Forms.Label Lblmensaje;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}

